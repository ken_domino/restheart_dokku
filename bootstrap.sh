#!/bin/bash
#
# Bootstrap Restheart from Dokku environment.

if [ "$PORT" == "" ] ; then
   export PORT=80
fi

if [ "$MONGO_URL" == "" ]; then
   export MONGO_URL="http://localhost"
fi

echo Port $PORT
echo Mongo URL $MONGO_URL

cat ./bootstrap.pre \
	| sed "s#mongo-uri: mongodb://127.0.0.1#mongo-uri: '$MONGO_URL'#" \
	| sed "s#http-port: 8080#http-port: $PORT#" \
	| sed "s#./etc/security.yml#/app/etc/security.yml#g" \
	> ./bootstrap.yml

java $JAVA_OPTS -jar ./restheart/target/restheart.jar bootstrap.yml

