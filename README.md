RESTHeart on Dokku
==================

This is a Dokku implementation of the RESTHeart server.

**RESTHeart** connects to **MongoDB** and opens data to the Web: Mobile and JavaScript applications can use the database via **RESTful HTTP API** calls.

Documentation
----

* RESTHeart Website: [http://restheart.org](http://restheart.org)

* RESTHeart Documentation: [https://softinstigate.atlassian.net/wiki/display/RH/Documentation](https://softinstigate.atlassian.net/wiki/x/l4CM)

* Dokku Website: [http://dokku.viewdocs.io/dokku/](http://dokku.viewdocs.io/dokku/)


Installation
---

RESTHeart can be installed on any OS supporting Java.

* Install Dokku on your OS.

* Install the MongoDB plug-in.



How to run it
---

> Running RESTHeart requires [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

Download the latest release from [github releases page](https://github.com/SoftInstigate/restheart/releases/latest), unpack the archive and just run the jar.

	$ java -server -jar restheart.jar

You might also want to specify a configuration file:

	$ java -server -jar restheart.jar etc/restheart.yml

> the restheart.yml configuration enables authentication: users, roles and permission are defined in etc/security.yml

* Configuration file [documentation](https://softinstigate.atlassian.net/wiki/x/JYCM)
* Example configuration file [restheart.yml](https://softinstigate.atlassian.net/wiki/x/VQC9)
* Security [documentation](https://softinstigate.atlassian.net/wiki/x/W4CM)

How to build it
---

> Building RESTHeart requires [Maven](http://www.oracle.com/technetwork/java/javase/downloads/index.html).

Clone the repository and update the git submodules (the __HAL browser__ is included in restheart as a submodule):

    $ git submodule update --init --recursive

Build the project with Maven:

    $ mvn clean package

Integration tests
---

Optionally you can run the integration test suite. Make sure __mongod is running__ on localhost on default port 27017 without authentication enabled, i.e. no `--auth` option is specified.

    $ mvn verify -DskipITs=false

Maven dependencies
---

RESTHeart's releases are available on [Maven Central](http://search.maven.org/#search%7Cga%7C1%7Cg%3A%22org.restheart%22).

Stable releases are available at:

https://oss.sonatype.org/content/repositories/releases/org/restheart/restheart/

If you want to embed RESTHeart in your project, add the dependency to your POM file:

```
<dependencies>
    <dependency>
        <groupId>org.restheart</groupId>
        <artifactId>restheart</artifactId>
        <version>1.1.4</version>
    </dependency>
</dependencies>
```

> Note that RESTHeart v 1.1.4 is the first release officially available on Maven Central.

Snapshot builds
----

Snapshots are available at:

https://oss.sonatype.org/content/repositories/snapshots/org/restheart/restheart/

If you want to build your project against a development release, first add the SNAPSHOT repository:

```
 <repositories>
    <repository>
         <id>restheart-mvn-repo</id>
         <url>https://oss.sonatype.org/content/repositories/snapshots</url>
         <snapshots>
             <enabled>true</enabled>
             <updatePolicy>always</updatePolicy>
         </snapshots>
    </repository>
 </repositories>
 ```

Then include the SNAPSHOT dependency in your POM:

```
<dependencies>
    <dependency>
        <groupId>org.restheart</groupId>
        <artifactId>restheart</artifactId>
        <version>1.2.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

Development releases are continually deployed to Maven Central by [Travis-CI](https://travis-ci.org/SoftInstigate/restheart).

## Project data

     391 text files.
     355 unique files.                                          
      86 files ignored.

	https://github.com/AlDanial/cloc v 1.66  T=3.65 s (84.1 files/s, 16382.3 lines/s)
	-------------------------------------------------------------------------------
	Language                     files          blank        comment           code
	-------------------------------------------------------------------------------
	Java                           218           4634           8798          18037
	JavaScript                      28           2672           2573          10913
	CSS                              3            988             26           6331
	XML                             39             59            135           2346
	Maven                            1             43              4            479
	YAML                             6            229            434            436
	JSON                             9              1              0            429
	HTML                             1             30              0            226
	Bourne Shell                     2              4              0              9
	-------------------------------------------------------------------------------
	SUM:                           307           8660          11970          39206

<hr></hr>

_Made with :heart: by [The SoftInstigate Team](http://www.softinstigate.com/). Follow us on [Twitter](https://twitter.com/softinstigate)_.